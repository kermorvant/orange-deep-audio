# -*- coding: utf-8 -*-
import numpy as np 
from keras import backend as K
from keras.preprocessing.image import Iterator
import pandas as pd
import librosa
import os
import multiprocessing.pool
from keras.preprocessing import sequence
from get_features import get_features as gf

def spect_loader(path, window_size, window_stride, window, normalize, max_len=101, logit=True):
    y, sr = librosa.load(path, sr=None)
    n_fft = int(sr * window_size)
    win_length = n_fft
    hop_length = int(sr * window_stride)
    D = librosa.stft(y, n_fft=n_fft, hop_length=hop_length,
                     win_length=win_length, window=window)
    spect, phase = librosa.magphase(D)

    if logit==True:
        spect = np.log1p(spect)
    if spect.shape[1] < max_len:
        pad = np.zeros((spect.shape[0], max_len - spect.shape[1]))
        spect = np.hstack((spect, pad))
    elif spect.shape[1] > max_len:
        spect = spect[:, :max_len]
    spect = np.resize(spect, (1, spect.shape[0], spect.shape[1]))
    if normalize:
        mean = np.mean(np.ravel(spect))
        std = np.std(np.ravel(spect))
        if std != 0:
            spect = spect -mean
            spect = spect / std

    return spect

def _count_valid_files_in_directory(directory, white_list_formats, follow_links):
    """Count files with extension in `white_list_formats` contained in a directory.
    # Arguments
        directory: absolute path to the directory containing files to be counted
        white_list_formats: set of strings containing allowed extensions for
            the files to be counted.
    # Returns
        the count of files with extension in `white_list_formats` contained in
        the directory.
    """
    samples = 0
    for root, dirs, files in os.walk(directory):
        for fname in files:
            is_valid = False
            for extension in white_list_formats:
                if fname.lower().endswith('.' + extension):
                    is_valid = True
                    break
            if is_valid:
                samples += 1
    return samples

def _list_valid_filenames_in_directory(directory, white_list_formats,
                                       class_indices, label_dict, follow_links):
    """List paths of files in `subdir` relative from `directory` whose extensions are in `white_list_formats`.

    # Arguments
        directory: absolute path to a directory containing the files to list.
            The directory name is used as class label and must be a key of `class_indices`.
        white_list_formats: set of strings containing allowed extensions for
            the files to be counted.
        class_indices: dictionary mapping a class name to its index.

    # Returns
        classes: a list of class indices
        filenames: the path of valid files in `directory`, relative from
            `directory`'s parent (e.g., if `directory` is "dataset/class1",
            the filenames will be ["class1/file1.jpg", "class1/file2.jpg", ...]).
    """
    classes = []
    filenames = []
    label_array = set()
    for root, dirs, files in os.walk(directory, topdown=False):
        for fname in files:
            is_valid = False
            for extension in white_list_formats:
                if fname.lower().endswith('.' + extension):
                    is_valid = True
                    break
            if is_valid:
                labels = pd.read_csv(os.path.join(root, fname.replace('.wav', '.csv')),
                                     names=['i', 'j', 'azimuth', 'elevation'])
                
                if fname not in class_indices:
                    class_indices[fname] = []
                    
                label = []
                for idx, item in labels.iterrows():
                    azimuth = np.around(item.azimuth, decimals=2)
                    elevation  = np.around(item.elevation, decimals=2)
                    if (azimuth, elevation) not in label_dict:
                        label_dict[(azimuth, elevation)] = len(label_dict)
                    label.append(label_dict[(azimuth, elevation)])
                    label_array.add((azimuth, elevation))

                absolute_path = os.path.join(root, fname)
#                import pdb;pdb.set_trace()
                classes.append(label)
                filenames.append(absolute_path)
    return classes, label_array, label_dict, filenames

class SpeechDirectoryIterator(Iterator):
    """Iterator capable of reading images from a directory on disk.
    # Arguments
       
    """

    def __init__(self, directory, window_size, window_stride, 
                 window_type, normalize, max_len=101, logit=True,
                 target_size=(256, 256), color_mode='grayscale',
                 classes=None, class_mode='categorical',
                 batch_size=32, shuffle=True, seed=None,
                 data_format=None, save_to_dir=None,
                 save_prefix='', save_format='png',
                 follow_links=False, interpolation='nearest'):
        if data_format is None:
            data_format = K.image_data_format()
        self.window_size = window_size
        self.window_stride = window_stride
        self.window_type = window_type
        self.normalize = normalize
        self.max_len = max_len
        self.directory = directory
        self.logit = logit
        self.target_size = tuple(target_size)
        self.color_mode = color_mode
        self.data_format = data_format
        if self.color_mode == 'rgb':
            if self.data_format == 'channels_last':
                self.image_shape = self.target_size + (3,)
            else:
                self.image_shape = (3,) + self.target_size
        else:
            if self.data_format == 'channels_last':
                self.image_shape = self.target_size + (1,)
            else:
                self.image_shape = (1,) + self.target_size
        self.classes = classes
        self.class_mode = class_mode
        self.save_to_dir = save_to_dir
        self.save_prefix = save_prefix
        self.save_format = save_format
        self.interpolation = interpolation

        white_list_formats = {'wav'}

        if not classes:
            classes = []
            for subdir in sorted(os.listdir(directory)):
                if os.path.isdir(os.path.join(directory, subdir)):
                    classes.append(subdir)
                    
        self.num_classes = len(classes)
        self.class_indices = {}
        
        pool = multiprocessing.pool.ThreadPool()
        results = []

        self.filenames = []
        self.classes = {}

        results.append(pool.apply_async(_list_valid_filenames_in_directory,
                                            (directory, white_list_formats,
                                             self.class_indices, self.classes, follow_links)))

        self.classes, self.label_array, self.label_dict, self.filenames = results[0].get()
    
        pool.close()
        pool.join()
        self.samples = len(self.filenames)
        print('Found %d wavs belonging to %d classes.' % (len(self.filenames), len(self.label_array)))

        super(SpeechDirectoryIterator, self).__init__(self.samples, batch_size, shuffle, seed)

    def _get_batches_of_transformed_samples(self, index_array):
        
        batch_x = []
        batch_y = []
        
        batch_f = []
        for i, j in enumerate(index_array):
            fname = self.filenames[j]
            label = self.classes[j]

            import keras
            if len(np.array(label).shape) == 1:
                for idx, y in enumerate(label):
                    label[idx] = keras.utils.to_categorical(y, num_classes=590)
            x = gf(fname)
            label = np.swapaxes(sequence.pad_sequences(np.swapaxes(label, 0, 1), 
                                                       maxlen=12,
                                         value=0.0,
                                         dtype='float32', padding="post"), 0, 1)
            
            batch_x.append(x)
            batch_y.append(label)
            
            batch_f.append(fname)

        return np.asarray(batch_x), np.asarray(batch_y)

    def next(self):
        """For python 2.x.
        # Returns
            The next batch.
        """
        with self.lock:
            index_array = next(self.index_generator)
        # The transformation of images is not under thread lock
        # so it can be done in parallel
        return self._get_batches_of_transformed_samples(index_array)