import librosa
import numpy as np
from scipy.io import loadmat
from math import sin, cos


def get_stft(wavname, srate=44100, nfft=2048, L=100, frm_time=0.04644):
    """
    From the article:
    The spectrogram is calculated for each of the audio channels sampled at 44100 kHz. A 2048-point
    discrete Fourier transform (DFT) is calculated on Hamming windows of 40 ms with 50 % overlap.
    We keep 1024 values of the DFT corresponding to the positive frequencies, without the zeroth bin.
    L frames of features, each containing 1024 magnitude and phase values of the DFT extracted in all the C channels,
    are stacked in a L × 1024 × 2C 3-D matrix and used as the input to the proposed neural network.
    The 2C dimension results from ordering the magnitude component of all channels first, followed by the phase.
    We use a sequence length L of 100 (= 2 s) in this work.
    """

    wlength = round(frm_time*srate) # window length (0.04644 instead of 0.04 to have wlength = 2**11)
    hlength = round(wlength/2)      # hop length

    Y, srate = librosa.core.load(wavname, sr=srate, mono=False)     # Load audio

    # Use lists for speed
    features = []
    stft_fts = []
    timestamps = []
    # Loop over all channels
    for ind in range(Y.shape[0]):
        y = Y[ind, :]
        # Compute STFT
        S = librosa.core.stft(y, n_fft=nfft, hop_length=hlength, win_length=wlength, window='hamming')
        # Extract magnitude and phase
        magnitude, phase = librosa.magphase(S)
        features.append(magnitude[1:, :])
        features.append(phase[1:, :])
        # Save also the complex version (needed for MUSIC)
        stft_fts.append(S)
        if ind == 0:
            timestamps = np.array(range(S.shape[-1] + 1)) * hlength/srate
            timestamps += (wlength/srate)

    # Transform into Numpy arrays
    features = np.array(features)
    stft_fts = np.array(stft_fts)
    # Calculate the number of groups (for segmentation)
    i = int(features.shape[-1] / L)

    # Trimming, dimension change (to accord with the format in the article) and segmentation
    features = features[:, :, :i * L]
    features = np.swapaxes(features, 0, 2)
    features = np.split(features, i)
    stft_fts = stft_fts[:, :, :i * L]
    stft_fts = np.swapaxes(stft_fts, 0, 2)
    stft_fts = np.split(stft_fts, i)

    timestamps = timestamps[:i* L]
    timestamps = np.split(timestamps, i)

    # Output (magnitude, phase) and complex stft features, and the timestamps of individual frames
    return features, stft_fts, timestamps


def steering_vectors(mat_fname):
    # Computes FOA steering (encoding) vectors

    # Load mat file with saved discretization scheme
    mat_data = loadmat(mat_fname)
    theta = np.ndarray.flatten(mat_data['theta_sp'])    # All azimuths (redundant)
    phi = np.ndarray.flatten(mat_data['phi_sp'])        # All elevations (redundant)

    steer_mtx = []      # Use list for speed
    for j in range(len(theta)):
        t = theta[j]
        t = np.deg2rad(t)   # Degrees to radians
        cos_t = cos(t)
        sin_t = sin(t)
        p = phi[j]
        p = np.deg2rad(p)   # Degrees to radians
        cos_p = cos(p)
        a = [1, cos_t*cos_p, sin_t*cos_p, sin(p)]   # Steering vector for the first order HOA
        steer_mtx.append(a)

    return np.array(steer_mtx), theta, phi


def music(stft_fts, steer_vcs, n_src):
    # MUltiple SIgnal Classification algorithm

    # Deduce sizs (num of frames, frequencies and channels)
    n_frames = stft_fts.shape[0]
    n_freqs = stft_fts.shape[1]
    n_chan = stft_fts.shape[2]
    # Inter-channel covariance matrix estimation
    cov_mtx = np.zeros((n_chan, n_chan), dtype=np.complex)
    for t in range(n_frames):
        for f in range(n_freqs):
            cov_mtx += np.outer(stft_fts[t,f,:],np.conjugate(stft_fts[t,f,:]))
        cov_mtx /= (n_frames + n_freqs)
    # Eigendecomposition
    w, U = np.linalg.eigh(cov_mtx)
    U = U[:, :n_chan - n_src]
    # MUSIC (inverse) power map
    yU = np.dot(steer_vcs, U)
    yU = np.sum(np.abs(yU)**2,axis=-1)
    # Output: MUSIC power map
    return 1/(yU + np.finfo(float).eps)


def get_features_music(fname, str_mtx, nsrc, nframes=100):
    # Computes all features from the wav file, based on the csv metadata

    # Get STFT features
    fts, stfts, tstamps = get_stft(fname, L=nframes)

    # Compute MUSIC features for each group of STFT fts
    num_ft = len(fts)   # Number of "groups"
    music_fts = []
    for j in range(num_ft):
        # Compute pseudospectrum
        curr_music = music(stfts[j], str_mtx, nsrc)
        # Dump data
        music_fts.append(curr_music)

    return fts, music_fts

def get_features(fname, str_mtx, nsrc, nframes=100):
    # Computes all features from the wav file, based on the csv metadata

    # Get STFT features
    fts, stfts, tstamps = get_stft(fname, L=nframes)

    return fts


if __name__ == "__main__":

    # Done once, offline
    A, theta_sp, phi_sp = steering_vectors('../Datasets/DOA/590pt.mat')

    # Compute both types of features, form .wav audio file
    # SUBOPTIMAL: the number of sources is fixed!!!
    o_stft_fts, all_musicfts = get_features('../Datasets/DOA/O2R/3/Train/5.wav', A, nsrc=2, nframes=10)

    # Only for display
    from matplotlib.tri import Triangulation
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    # Delaunay triangulation, used only for displaying non-uniform data
    tri = Triangulation(theta_sp, phi_sp)

    # Interactive mode for instant plot in figure 'instant_plot'
    plt.interactive(False)
    fig = plt.figure(num='MUSIC')
    fig.subplots_adjust(left=0, right=1, bottom=0, top=1)

    for f in range(len(all_musicfts)):
        s = all_musicfts[f]
        plt.clf()
        ax = fig.gca(projection='3d')
        ax.view_init(azim=-90, elev=-90)
        plt.axis('off')
        ax.plot_trisurf(tri, s, cmap='viridis', linewidth=0, antialiased=False)

        # Python seems confused with remote desktop kybd/mouse (waitforbuttonpress does not work properly)
        plt.title('Feature ' + str(f))
        plt.pause(0.1)

        # plt.title('Press any key to continue')
        # ispressed = plt.waitforbuttonpress()
        # print(ispressed)
