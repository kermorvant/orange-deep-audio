# -*- coding: utf-8 -*-
from keras.models import Input
from keras.layers.core import Dense
from keras.layers import Reshape, Conv2D, MaxPooling2D, GlobalMaxPooling2D
from keras.layers import TimeDistributed, Bidirectional, BatchNormalization
import numpy as np
from keras.layers.recurrent import GRU
from keras.models import Model
from audio import SpeechDirectoryIterator
import keras.backend as K
from keras.callbacks import TensorBoard
import datetime
import argparse

window_size=.02
window_stride=.01
window_type='hamming'
normalize=True
max_len=101

def get_model(train_iterator):

    main_input = Input(shape=(None, 100, 1024, 8), name='main_input')

    x = TimeDistributed(Conv2D(64, (3, 3), padding="same",
                                  activation='relu',
                                  data_format="channels_last",
                                  kernel_initializer='he_normal'))(main_input)
    x = TimeDistributed(MaxPooling2D(pool_size=(1, 8)), name="maxpool1")(x)
    x = BatchNormalization()(x)
    x = TimeDistributed(Conv2D(64, (3, 3), padding="same",
                                  activation='relu',
                                  data_format="channels_last",
                                  kernel_initializer='he_normal'))(x)
    x = TimeDistributed(MaxPooling2D(pool_size=(1, 8)), name="maxpool2")(x)
    x = BatchNormalization()(x)
    x = TimeDistributed(Conv2D(64, (3, 3), padding="same",
                                  activation='relu',
                                  data_format="channels_last",
                                  kernel_initializer='he_normal'))(x)
    x = TimeDistributed(MaxPooling2D(pool_size=(1, 4)), name="maxpool3")(x)
    x = BatchNormalization()(x)
    x = TimeDistributed(Conv2D(64, (3, 3), padding="same",
                                  activation='relu',
                                  data_format="channels_last",
                                  kernel_initializer='he_normal'))(x)

    x = TimeDistributed(MaxPooling2D(pool_size=(1, 2)), name="maxpool4")(x)
    x = TimeDistributed(GlobalMaxPooling2D())(x)

    x = Bidirectional(GRU(64, return_sequences=True))(x)
    x = Bidirectional(GRU(64, return_sequences=True))(x)
    x = TimeDistributed(Dense(614))(x)
    x = Reshape((-1, K.shape(x)[1], K.shape(x)[2], 1), input_shape=x.shape)(x)
    x = TimeDistributed(Conv2D(16, (3, 3), padding="same",
                                  activation='relu',
                                  data_format="channels_last",
                                  kernel_initializer='he_normal'))(x)
    x = TimeDistributed(MaxPooling2D(pool_size=(1, 2)))(x)
    x = BatchNormalization()(x)
    x = TimeDistributed(Conv2D(16, (3, 3), padding="same",
                                  activation='relu',
                                  data_format="channels_last",
                                  kernel_initializer='he_normal'))(x)

    x = TimeDistributed(GlobalMaxPooling2D())(x)
    x = TimeDistributed(Dense(32))(x)

    x = Bidirectional(GRU(16, return_sequences=True, activation='tanh'))(x)
    x = Bidirectional(GRU(16, return_sequences=True, activation='tanh'))(x)
    
    doa_output = TimeDistributed(Dense(590, activation='softmax', name='doa_output'))(x)
    
    model = Model(inputs=[main_input], outputs=[doa_output])
    
    model.compile(optimizer='adam',
              loss='categorical_crossentropy', 
                          metrics=['accuracy'])
    model.summary()
    return model

def run(run_name, train_path, valid_path, test_path, batch_size, epochs):
    
    train_iterator = SpeechDirectoryIterator(directory=train_path, 
                                       batch_size=batch_size, 
                                       window_size=window_size, 
                                       window_stride=window_stride, 
                                       window_type=window_type,
                                       normalize=normalize, 
                                       max_len=max_len, 
                                       shuffle=False)
    
    model = get_model(train_iterator)
    test_iterator = SpeechDirectoryIterator(directory=test_path, 
                                       batch_size=batch_size, 
                                       window_size=window_size, 
                                       window_stride=window_stride, 
                                       window_type=window_type,
                                       normalize=normalize, 
                                       max_len=max_len, 
                                       classes=None,
                                       shuffle=False)
    
    model.fit_generator(train_iterator,
            steps_per_epoch=np.ceil(train_iterator.n/batch_size),
            epochs=epochs,
            verbose=1,
            callbacks=[TensorBoard(log_dir='logs/' + run_name)])
    
    scores = model.evaluate_generator(generator=test_iterator, 
                                      steps=int(np.ceil(test_iterator.n)/batch_size))
    
    print("Accuracy:", scores[1]*100.0, '%')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Speech Recognition Example')

    parser.add_argument(
        '-train', '--train-directory', metavar='TRAIN',
        required=True,
        help='The directory with the sound files for train')
    parser.add_argument(
        '-test', '--test-directory', metavar='TEST',
        required=True,
        help='The directory with the sound files for test')
    parser.add_argument(
        '-val', '--val-directory', metavar='VAL',
        required=True,
        help='The directory with the sound files for validation')
    parser.add_argument(
        '-b', '--batch-size', metavar='batch_size',
        default=64,
        type=int,
        help='batch size for training')
    parser.add_argument(
        '-e', '--epochs', metavar='epochs',
        default=2,
        type=int,
        help='Epochs')
    parser.add_argument(
        '-r', '--run', metavar='run',
        default='',
        help='run')

    args = parser.parse_args()

    run_name = args.train_directory.replace(r'/', '_').replace('.', '') + \
        str(datetime.datetime.now()).replace(' ', '_').split(':')[0] + '_'  \
        + str(args.batch_size) + '_' + str(args.run)

    run(run_name, args.train_directory, args.val_directory,
        args.test_directory, args.batch_size, args.epochs)
    




