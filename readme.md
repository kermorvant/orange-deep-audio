# ORANGE

Python 3.*

## Libraries

$ pip install -r requirements

## Training the model

$ python train.py -train TRAIN_DIR -test TEST_DIR -val VALID_DIR -b 16 -e 32 -r MY_RUN

TRAIN_DIR, TEST_DIR, VALID_DIR: directories with sound files in *.wav format and *.csv files with features

-b batch size
-e epochs
-r the name of the run (personalized)
