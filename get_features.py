# -*- coding: utf-8 -*-

import librosa
import numpy as np

def get_features(wavname, srate=44100, nfft=2048, L=100, frm_time=0.04644):
    """
    The spectrogram is calculated for each of the audio channels sampled at 44100 kHz. A 2048-point
    discrete Fourier transform (DFT) is calculated on Hamming windows of 40 ms with 50 % overlap.
    We keep 1024 values of the DFT corresponding to the positive frequencies, without the zeroth bin.
    L frames of features, each containing 1024 magnitude and phase values of the DFT extracted in all the C channels,
    are stacked in a L × 1024 × 2C 3-D matrix and used as the input to the proposed neural network.
    The 2C dimension results from ordering the magnitude component of all channels first, followed by the phase.
    We use a sequence length L of 100 (= 2 s) in this work.
    """

    wlength = round(frm_time*srate) # 0.04644 instead of 0.04 to have wlength = 2**11
    hlength = round(wlength/2)

    Y, srate = librosa.core.load(wavname, sr=srate, mono=False)
    
    features = []
    for ind in range(Y.shape[0]):
        y = Y[ind, :]
        S = librosa.core.stft(y, n_fft=nfft, hop_length=hlength, win_length=wlength, window='hamming')
        magnitude, phase = librosa.magphase(S)
        features.append(magnitude[1:, :])
        features.append(phase[1:, :])

    features = np.array(features)
    i = int(features.shape[-1]/L)
    features = features[:, :, :i * L]
    features = np.swapaxes(features, 0, 2)
    features = np.split(features, i)

    return features

